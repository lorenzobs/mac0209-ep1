# Algoritmo que recebe o nome de um arquivo json retirado da API do KartaView
# e extrai as informações relevantes para análise futura

# É necessário instalar localmente o pacote pyproj para realizar a 'limpeza'
# inicial dos dados dos pontos  

import json
import sys
from pyproj import Proj, transform
import warnings 
warnings.filterwarnings("ignore")

def get_photo_array_positions(txt):
    '''
    Função Auxiliar. Devolve uma tupla contendo, respectivamente,
    o caractere em que começa a seção a ser extraída da string <txt>,
    o vetor de "photos" neste caso, e o último caractere (da mesma
    string <txt>) de interesse.
    '''

    s = txt.index("\"photos\":[{")
    e = txt.index("}]", s + 1)
    return s, e + len("}]")

def make_extract_photos_JSON(output_file, json_msg):
    '''
    Dada uma string <json_msg>, extraí o vetor "photos" do objeto
    json correpondente usando a função auxiliar get_photo_array_positions()
    e o armazena em um arquivo <output_file>.json, também devolvendo o resultado.
    '''
    first_pos, last_pos = get_photo_array_positions(json_msg)
    extracted_str = "{" + json_msg[first_pos:last_pos] + "}"
    with open(output_file, "w") as ejf:
        ejf.write(extracted_str)
    return extracted_str

def clean_extracted(txt):
    '''
    Limpa uma string <txt> presumidamente retirada de um arquivo .json
    sobre o qual já foi aplicado o método make_extract_photos_JSON().
    Devolve uma string em não parseada de json com os dados devidamente 
    limpos e com as adições dignas de análise.
    '''
    clean = '{"photos":['  
    jsonobj = json.loads(txt)
    porcento = 0
    print("Loading:",porcento,"%")
    tam = len(jsonobj["photos"])

    modo = 0
    obj = jsonobj["photos"][0]
    new_easting, new_northing = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'),float(obj["lat"]),float(obj["lng"]))
    if (new_easting >= float("inf") or new_northing >= float("inf")):
        modo = 1;

    for i in range(len(jsonobj["photos"])):
        clean = clean + "{"

        obj = jsonobj["photos"][i]
        if modo == 0:
            new_easting, new_northing = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'),float(obj["lat"]),float(obj["lng"]))
        else:
            new_northing,new_easting = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'),float(obj["lng"]),float(obj["lat"]))
        parcial = (i*100)//tam
        if ( parcial > porcento):
            porcento = porcento + 1           
            print("Loading:",porcento,"%")            

        clean = clean + '"lat":' + '"' + obj["lat"] + '",'
        clean = clean + '"lng":' + '"' + obj["lng"] + '",'
        clean = clean + '"easting":' + '"' + str(new_easting) + '",'
        clean = clean + '"northing":' + '"' + str(new_northing) + '",'
        clean = clean + '"heading":' + '"' + obj["heading"] + '",'
        clean = clean + '"shot_date":' + '"' + obj["shot_date"] + '"'
        if (i == len(jsonobj["photos"])-1):
            clean = clean + "}"
        else:
            clean = clean + "},"
    clean = clean + ']}'
    print("Loading: 100%")
    return clean

def main():
    arquivo_json = sys.argv[1]
    extracted_filename = "extracted_" + arquivo_json
    cleaned_filename = "cleaned_" + arquivo_json
    with open(arquivo_json, "r") as jf:
            txt = jf.read()
            extracted_str = make_extract_photos_JSON(extracted_filename, txt)
            with open(cleaned_filename, "w") as cjf:
                cjf.write(clean_extracted(extracted_str))

main()
