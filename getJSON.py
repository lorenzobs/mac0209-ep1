# Algoritmo que recebe certo TrackID do site Kartaview, coleta os dados desse trecho
# e os salva em um arquivo cujo nome também é fornecido na entrada padrão

import requests

url = 'https://api.openstreetcam.org/details'

trackid = input("Insira o trackid: ")
output_file = input("Insira o nome do arquivo de saída: ")

body = {'id' : trackid }
x = requests.post(url,data = body)


if (output_file == ""):
	output_file = "response_" + trackid + ".json"

with open(output_file, "w") as ejf:
        ejf.write(x.text)

