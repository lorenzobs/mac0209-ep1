#  Recebe da linha de comando o nome do arquivo .json que contém os
# dados dos pontos (supostamente já "limpos" pelo script extract.py)
# de um trajeto e conduz as análises desejadas sobre eles.

import json
import sys
from math import pi, sin, cos, asin, acos, sqrt
import matplotlib.pyplot as pyplot
from datetime import datetime
import numpy as np

def get_haversine_dist(p1,p2):
	'''
	Devolve a distância entre dois pontos, conforme calculada pela 
	fórmula de Haversine. 
	'''

	R = 6.371*(10**6) # Raio da Terra ~ 6371 KM

	lat1 = float(p1["lat"])*pi/180
	lat2 = float(p2["lat"])*pi/180
	lng1 = float(p1["lng"])*pi/180
	lng2 = float(p2["lng"])*pi/180

	senao = sqrt( sin( abs(lat2 - lat1) /2 ) **2 + cos(lat1)*cos(lat2)*(sin( abs(lng2 - lng1) /2 )**2))

	return 2*R*asin(senao)

def get_spherical_trig_dist(p1,p2):
	'''
	Devolve a distância entre dois pontos calculada usando trigono-
	metria esférica. 
	'''

	R = 6.371*(10**6) # Raio da Terra ~ 6371 KM

	lat1 = float(p1["lat"])*pi/180
	lat2 = float(p2["lat"])*pi/180
	lng1 = float(p1["lng"])*pi/180
	lng2 = float(p2["lng"])*pi/180

	Cossenao = sin(abs(lat1))*sin(abs(lat2)) + cos(lng2 - lng1)*cos(lat1)*cos(lat2)

	return (acos(Cossenao)*R)

def get_mercator_dist(p1,p2):
	'''
	Devolve a distância entre dois pontos, calculando a distância 
	euclidiana entre eles usando as propriedades easting e northing
	dos pontos.
	'''
	return sqrt((float(p1["easting"]) - float(p2["easting"]) )**2+(float(p1["northing"]) - float(p2["northing"]))**2)

def compara_calculos(pontos):

	'''
	Recebe um vetor com todos os pontos, calcula a distância entre cada par deles por
	meio dos 3 métodos, (Harversine,spherical_trig e Mercator) e plota os gráficos
	comparando os cálculos entre si.
	'''
	dist_haver = []
	dist_trig = []
	dist_merc = []
	err_h_t = []
	err_h_m = []
	err_t_m = []
	x = []
	for i in range(0,len(pontos)-1):
		x.append(i)
		d_h = get_haversine_dist(pontos[i],pontos[i+1])
		d_t = get_spherical_trig_dist(pontos[i],pontos[i+1])
		d_m = get_mercator_dist(pontos[i],pontos[i+1])
		err_h_t.append(abs(d_h-d_t))
		err_h_m.append(abs(d_h-d_m))
		err_t_m.append(abs(d_t-d_m))
		dist_haver.append(d_h)
		dist_trig.append(d_t)
		dist_merc.append(d_m)

	grafico = input("Deseja ver os gráficos das distâncias entre cada par de pontos?(S/N) ")
	while (grafico != "N" and grafico != "S"):
		grafico = input("Deseja ver os gráficos das distâncias entre cada par de pontos?(S/N) ")

	if (grafico == "N"):
		return

	pyplot.figure(0)
	pyplot.scatter(x,dist_haver,2, color='red',label='Haversine' )
	pyplot.scatter(x,dist_trig,2, color='blue',label='Trigonométrica' )
	pyplot.scatter(x,dist_merc,2, color='green',label='Mercator')
	pyplot.legend()
	pyplot.title("Distância entre cada par de pontos")
	pyplot.xlabel("Índice")
	pyplot.ylabel("Distância(m)")
	
	pyplot.figure(1)
	pyplot.plot(err_h_t)
	pyplot.title("Diferença entre a projeção de Haversine e a trigonométrica")
	pyplot.xlabel("Índice")
	pyplot.ylabel("Diferença(m)")

	pyplot.figure(2)
	pyplot.plot(err_h_m)
	pyplot.title("Diferença entre a projeção de Haversine e a de Mercator")
	pyplot.xlabel("Índice")
	pyplot.ylabel("Diferença(m)")
	
	pyplot.figure(3)
	pyplot.plot(err_t_m)
	pyplot.title("Diferença entre a projeção trigonométrica e a de Mercator")
	pyplot.xlabel("Índice")
	pyplot.ylabel("Diferença(m)")


	pyplot.show()

	return


def converte_tempo(tempo):
	'''
	Função auxiliar que recebe uma data/hora e as converte
	para o tipo timedelta
	'''
	FMT = '%Y-%m-%d %H:%M:%S'
	t = datetime.strptime(tempo,FMT)
	return t

def projeta_velocidade(pontos):
	'''
	Função que recebe o vetor de pontos e calcula a velocidade em cada ponto,
	considerando os três métodos de cálculo de distâncias e plota as velocidades
	em três gráficos.
	'''
	t = []
	v_haver = []
	v_trig = []
	v_merc = []
	dist_haver = [0]
	dist_trig = [0]
	dist_merc = [0]
	tempo = 0
	for i in range(0,len(pontos)-1):

		deltaT = converte_tempo(pontos[i+1]['shot_date'])-converte_tempo(pontos[i]['shot_date'])
		deltaT  = deltaT.seconds
		tempo = tempo + deltaT
		t.append(tempo)

		d_h = get_haversine_dist(pontos[i],pontos[i+1])
		d_t = get_spherical_trig_dist(pontos[i],pontos[i+1])
		d_m = get_mercator_dist(pontos[i],pontos[i+1])

		dist_haver.append(dist_haver[len(dist_haver)-1]+d_h)
		dist_trig.append(dist_trig[len(dist_trig)-1]+d_t)
		dist_merc.append(dist_trig[len(dist_merc)-1]+d_m)

		if (deltaT != 0):
			v_haver.append(d_h/deltaT)
			v_trig.append(d_t/deltaT)
			v_merc.append(d_m/deltaT)
		# Se o deltaT for 0, podemos considerar que a velocidade não mudou desde o último ponto
		elif (v_haver != []): 
			v_haver.append(v_haver[len(v_haver)-1])
			v_trig.append(v_trig[len(v_trig)-1])
			v_merc.append(v_merc[len(v_merc)-1])	

	vel_constante = acha_trecho_constante(v_haver)

	velocidades = np.array([v_haver,v_trig,v_merc])
	distancias = np.array([dist_haver,dist_trig,dist_merc])
	calculos_trechos_constantes(vel_constante,velocidades,distancias,t)

	grafico = input("Deseja ver os gráficos das velocidades em todo trecho?(S/N) ")
	while (grafico != "N" and grafico != "S"):
		grafico = input("Deseja ver os gráficos das velocidades em todo trecho?(S/N) ")

	if (grafico == "N"):
		return

	pyplot.figure(1)
	pyplot.plot(t,v_haver)
	pyplot.title("Velocidade no método de Haversine")
	pyplot.xlabel("Tempo(s)")
	pyplot.ylabel("Velocidade(m/s)")

	pyplot.figure(2)
	pyplot.plot(t,v_trig)
	pyplot.title("Velocidade no método trigonométrico")
	pyplot.xlabel("Tempo(s)")
	pyplot.ylabel("Velocidade(m/s)")

	pyplot.figure(3)
	pyplot.plot(t,v_merc)
	pyplot.title("Velocidade no método de Mercator")
	pyplot.xlabel("Tempo(s)")
	pyplot.ylabel("Velocidade(m/s)")

	pyplot.show()

	return

def acha_trecho_constante(v):
	'''
	Função que recebe um vetor de velocidades e encontra subtrechos com velocidade
	constante e devolve uma lista de arrays da forma [ponto inicial, ponto final],
	que representa o ínicio e o fim de cada subtrecho.
		Critérios para considerar um subtrecho constante:
			- Subtrecho deve possuir mais de 20 pontos
			- A diferença de velocidade entre cada ponto do subtrecho e o ponto inicial
			deve ser menor que 1m/s  
	'''
	vel_constante = []
	inicio = 0
	# Cálculo dos subtrechos com velocidade constante
	for i in range(0,len(v)):
		if (v[inicio] == 0):
			v[inicio] = v[inicio+1]		
		razao = abs(v[i] - v[inicio])/abs(v[inicio])
		criterio = 0.1
		if (razao >= criterio or ( razao < criterio and i == (len(v)-1) )):
			fim = i
			if (abs(fim - inicio) > 20):
				trecho = np.array([inicio,fim])
				vel_constante.append(trecho)
			inicio = i + 1

	# Cálculo da média de pontos dentro de cada subtrecho
	media = 0
	for i in range(0,len(vel_constante)):
		trecho = vel_constante[i]
		media = media + (trecho[1]-trecho[0] + 2) 
	media = media / len(vel_constante)
	print("Foram encontrados",len(vel_constante),"subtrechos com velocidade constante")
	print("A quantidade média de pontos dentro de cada subtrecho é:",media)
	return vel_constante

def projeta_distancia(pontos):
	'''
	Função que recebe o vetor de pontos, calcula a distância percorrida no trecho inteiro
	e a projeta graficamente
	'''	
 	
	d_haver = []
	d_trig = []
	d_merc = []
	tempo = 0
	t = []
	d_h = 0
	d_t = 0
	d_m = 0
	for i in range(0,len(pontos)-1):

		deltaT = converte_tempo(pontos[i+1]['shot_date'])-converte_tempo(pontos[i]['shot_date'])
		deltaT  = deltaT.seconds
		tempo = tempo + deltaT
		t.append(tempo)

		d_h += get_haversine_dist(pontos[i],pontos[i+1])
		d_t += get_spherical_trig_dist(pontos[i],pontos[i+1])
		d_m += get_mercator_dist(pontos[i],pontos[i+1])
		d_haver.append(d_h)
		d_trig.append(d_t)
		d_merc.append(d_m)

	grafico = input("Deseja ver os gráficos das distâncias em todo trecho?(S/N) ")
	while (grafico != "N" and grafico != "S"):
		grafico = input("Deseja ver os gráficos das distâncias em todo trecho?(S/N) ")

	if (grafico == "N"):
		return

	pyplot.figure(1)
	pyplot.plot(t,d_haver)
	pyplot.title("Distância no método de Haversine")
	pyplot.xlabel("Tempo(s)")
	pyplot.ylabel("Distância(m)")

	pyplot.figure(2)
	pyplot.plot(t,d_trig)
	pyplot.title("Distância no método de trigonométrico")
	pyplot.xlabel("Tempo(s)")
	pyplot.ylabel("Distância(m)")
	pyplot.figure(3)
	pyplot.plot(t,d_merc)
	pyplot.title("Distância no método de Mercator")
	pyplot.xlabel("Tempo(s)")
	pyplot.ylabel("Distância(m)")

	pyplot.show()
	return

def calculos_trechos_constantes(trechos,velocidades,distancias,tempo):
	'''
	Função que faz os cálculos de velocidade,distância e tempo para cada um dos subtrechos de velocidade constante
		Trechos é o vetor que contem o inicios e fim de cada um dos subtrechos. Ex:inicio = trechos[0][0] fim = trecho[0][1]
		Velocidades é um vetor que contém as velocidades do trecho inteiro medidas nos três métodos: Haversine = velocidades[0],
		Trigonometrica = velocidades[1], Mercator = velocidades[2]
		Distâncias é o vetor que contém as distâncias em cada ponto do trecho total, calculadas por meio dos três métodos. Se comporta como o vetor velocidades.
		Tempo é o vetor com os tempos em cada ponto do trecho inteiro.
	'''
	v_haver = velocidades[0]
	v_trig = velocidades[1]
	v_merc = velocidades[2]
	d_haver = distancias[0]
	d_trig = distancias[1]
	d_merc = distancias[2]


	for i in range(0,len(trechos)):
		inicio = trechos[i][0] # inicio do subtrecho
		fim = trechos[i][1] # fim do subtrecho 

		# Cálculo do tempo total
		if(inicio >= 1):
			t_total = tempo[fim]-tempo[inicio-1]
		else:
			t_total = tempo[fim]
		
		#Cálculo da distância total
		d_total_h = d_haver[fim] - d_haver[inicio] + (tempo[inicio]*v_haver[inicio])
		d_total_t = d_trig[fim] - d_trig[inicio] + (tempo[inicio]*v_trig[inicio])
		d_total_m = d_merc[fim] - d_merc[inicio] + (tempo[inicio]*v_merc[inicio])

		# Cálculo da velocidade média
		v_media_h = d_total_h / t_total
		v_media_t = d_total_t / t_total
		v_media_m = d_total_m / t_total

		print("SUBTRECHO",i,":")
		print("\tDistância:")
		print("\t%.2f m (H) / "% d_total_h,end='')
		print("%.2f m (T) / " % d_total_t,end='')
		print("%.2f m (M)" % d_total_m)	
		print("\tVelocidades médias:")
		print("\t%.2f m/s (H) / "% v_media_h,end='')
		print("%.2f m/s (T) / " % v_media_t,end='')
		print("%.2f m/s (M)" % v_media_m)		
		print("\tTempo total:",t_total, "s")


		#Distância entre cada par de pontos
		d_pontos_h = []
		d_pontos_t = []
		d_pontos_m = []

		#Tempo entre cada par de pontos
		t_pontos = []

		#Velocidade entre cada par de pontos
		v_pontos_h = []
		v_pontos_t = []
		v_pontos_m = []
		for j in range(inicio,fim-1):

			# Cálculo da velocidade entre cada par de pontos
			d_pontos_h.append(d_haver[j+1] - d_haver[j])
			d_pontos_t.append(d_trig[j+1] - d_trig[j])
			d_pontos_m.append(d_merc[j+1] - d_merc[j])

			# Cálculo da distância entre cada par de pontos
			v_pontos_h.append(v_haver[j+1] - v_haver[j])
			v_pontos_t.append(v_trig[j+1] - v_trig[j])
			v_pontos_m.append(v_merc[j+1] - v_merc[j])

			# Cálculo do tempo entre cada par de pontos
			t_pontos.append(tempo[j+1]-tempo[j]) 





def main():

	if len(sys.argv) != 2:
		print("Modo de execução: \n\t python analyse.py <filename> \nOnde <filename> é o nome do arquivo .json com os pontos a serem analisados")
		return -1

	arquivo_json = sys.argv[1]
	
	with open(arquivo_json, "r") as jf:
		texto = jf.read()
		pontos_json = json.loads(texto)
		pontos_json = pontos_json["photos"]
		compara_calculos(pontos_json)
		projeta_velocidade(pontos_json)
		projeta_distancia(pontos_json)

main()
